package valiit.objectcomparison;

import java.time.LocalDate;
import java.util.Objects;

public class Person {
    private String firstName;
    private String lastName;
    private int weight;
    private int height;
    private String eyeColor;
    private LocalDate dateOfBirth;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Person(String firstName, String lastName, int weight, int height, String eyeColor, LocalDate dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.weight = weight;
        this.height = height;
        this.eyeColor = eyeColor;
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }

        Person other = (Person)o;
        return this.getFirstName().equals(other.getFirstName()) &&
                this.getLastName().equals(other.getLastName()) &&
                this.getDateOfBirth().equals(other.getDateOfBirth());
    }

    @Override
    public int hashCode() {
        int hash = 11;
        hash = hash * 97 + Objects.hashCode(this.firstName);
        hash = hash * 97 + Objects.hashCode(this.lastName);
        hash = hash * 97 + Objects.hashCode(this.dateOfBirth);
        return hash;
    }
}
