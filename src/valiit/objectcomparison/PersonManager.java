package valiit.objectcomparison;

import java.time.LocalDate;

public class PersonManager {
    public static void main(String[] args) {
        Person p1 = new Person(
                "Mati", "Murakas", 67, 175,
                "blue", LocalDate.of(1980, 3, 1)
        );

        Person p2 = new Person(
                "Mati", "Murakas", 71, 175,
                "red", LocalDate.of(1980, 3, 1)
        );

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p1.equals(p2));
    }
}
