package valiit.visitors;


import java.io.IOException;


public class App {
    public static void main(String[] args) throws IOException {

        Visitors.loadVisitList(args[0]);
        Visitors.sortByVisitNumber();

    }
}
