package valiit.visitors;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

class Visitors {
    private static List<Visit> visitList = new ArrayList<>();

    protected static void loadVisitList(String visitorFilePath) throws IOException {
        List<String> visitLines = Files.readAllLines(Paths.get(visitorFilePath));
        for (int i = 0; i < visitLines.size(); i++) {
            String[] visitInfo = visitLines.get(i).split(", ");
            String date = visitInfo[0];
            int visitCount = Integer.parseInt(visitInfo[1]);
            Visit visit = new Visit(date, visitCount);
            visitList.add(visit);
        }
    }

    protected static void sortByVisitNumber() {
        visitList.sort((x1, x2) -> x1.getVisitCount() - x2.getVisitCount());
        System.out.println(Visitors.visitList);
        Visit mostVisitors = visitList.get(visitList.size() - 1);
        System.out.println(mostVisitors);
    }
}








