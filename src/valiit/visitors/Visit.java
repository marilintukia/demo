package valiit.visitors;

public class Visit {
    String date;
    int visitCount;

    public String getDate() {
        return date;
    }

    public int getVisitCount() {
        return visitCount;
    }

    public Visit(String date, int visitCount) {
        this.date = date;
        this.visitCount = visitCount;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "date='" + date + '\'' +
                ", visitCount=" + visitCount +
                '}';
    }
}
