package valiit;

public class Day03Exercises {

    public static final double VAT_RATE = 1.2;
    public static final String LANG_ESTONIAN = "ET";
    public static final String LANG_LATVIAN = "LT";
    public static final String DATE_FORMAT_ET = "dd.mm.yyyy hh:MM:ss";

    public static void main(String[] args) {
        // Ülesanne 2
        String city = args[0];
        if (city.equals("Milano")) {
            System.out.println("Ilm on soe.");
        } else {
            System.out.println("Ilm polegi kõige tähtsam!");
        }

        // Ülesanne 3
        int grade = Integer.parseInt(args[1]);
        if (grade == 1) {
            System.out.println("nõrk");
        } else if (grade == 2) {
            System.out.println("mitterahuldav");
        } else if (grade == 3) {
            System.out.println("rahuldav");
        } else if (grade == 4) {
            System.out.println("hea");
        } else if (grade == 5) {
            System.out.println("suurepärane");
        } else {
            System.out.println("Viga! Tundmatu hinne!");
        }

        // Ülesanne 4
        switch (args[1]) {
            case "1":
                System.out.println("nõrk");
                break;
            case "2":
                System.out.println("mitterahuldav");
                break;
            case "3":
                System.out.println("rahuldav");
                break;
            case "4":
                System.out.println("hea");
                break;
            case "5":
                System.out.println("suurepärane");
                break;
            default:
                System.out.println("Viga! Tundmatu hinne!");
        }

        // Ülesanne 5
//        int personAge = 78;
        int personAge = Integer.parseInt(args[1]);
        String ageGroup = personAge > 100 ? "vana" : "noor";
        System.out.println(ageGroup);
//        System.out.println(personAge > 100 ? "vana" : "noor");

        // Ülesanne 6
        // Ebamõistlik konstruktsioon (inline if ei ole siin soovitatav)
        ageGroup = (personAge > 100) ?
                "vana"
                :
                ((personAge == 100) ? "peaaegu vana" : "noor");

        // Soovituslik lahendus
        if (personAge > 100) {
            ageGroup = "vana";
        } else if (personAge == 100) {
            ageGroup = "peaaegu vana";
        } else {
            ageGroup = "noor";
        }

        System.out.println(ageGroup);

        double productPrice = 56.99;
        double productRetailPrice = productPrice * VAT_RATE;
        System.out.println(productRetailPrice);

    }
}

