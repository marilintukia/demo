package valiit.bank;

import java.io.IOException;
import java.time.LocalDate;

public class BankAccountsFileNotFoundException
    extends IOException {

    private LocalDate time;

    public BankAccountsFileNotFoundException(String message, LocalDate time) {
        super(message);
        this.time = time;
    }

    public LocalDate getCreationTime() {
        return this.time;
    }
}
