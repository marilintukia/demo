package valiit.bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

class AccountService {
    private static List<Account> accounts = new ArrayList<>();

    protected static void loadAccounts(String accountFilePath) throws BankAccountsFileNotFoundException {
        try {
            List<String> accountLines =
                    Files.readAllLines(Paths.get(accountFilePath));
            for (int i = 0; i < accountLines.size(); i++) {
                String[] accountLineParts = accountLines.get(i).split(", ");
                String firstName = accountLineParts[0];
                String lastName = accountLineParts[1];
                String number = accountLineParts[2];
                double balance = Double.parseDouble(accountLineParts[3]);
                Account account = new Account(firstName, lastName, number, balance);
                accounts.add(account);
            }
        } catch (IOException e) {
            throw new BankAccountsFileNotFoundException(
                    "The specified accounts file was not found: " + accountFilePath,
                    LocalDate.now());
        }
    }

    protected static Account findAccount(String number) {
        for (int i = 0; i < accounts.size(); i++) {
            Account a = accounts.get(i);
            if (a.getNumber().equals(number)) {
                return a;
            }
        }
        return null;
    }

    protected static Account findAccount(String firstName, String lastName) {
        for (Account a : accounts) {
            if (a.getFirstName().equalsIgnoreCase(firstName) && a.getLastName().equalsIgnoreCase(lastName)) {
                return a;
            }
        }
        return null;
    }

    protected static TransferResult transfer(String fromAccNumber, String toAccNumber, double sum) {
        Account fromAccount = findAccount(fromAccNumber);
        Account toAccount = findAccount(toAccNumber);
        if (fromAccount != null && toAccount != null) {
            if (fromAccNumber.equals(toAccNumber)) {
                return new TransferResult(false, "Sender and beneficiary account numbers must be different.");
            }

            sum = Math.abs(sum);
            if (fromAccount.getBalance() >= sum) {
                double fromAccountBalance = fromAccount.getBalance() - sum;
                double toAccountBalance = toAccount.getBalance() + sum;
                fromAccount.setBalance(fromAccountBalance);
                toAccount.setBalance(toAccountBalance);
                return new TransferResult(
                        true, "OK", fromAccount, toAccount
                );
            } else {
                return new TransferResult(false, "Not enough funds.");
            }
        } else {
            String errorMessage = fromAccount == null ?
                    "Sender account not found." : "Beneficiary account not found.";
            return new TransferResult(false, errorMessage);
        }
    }

}
