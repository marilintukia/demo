package valiit.cryptoapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException {
        List<String> alphabet = Files.readAllLines(Paths.get(args[0]));
        Encryptor encryptor = new Encryptor(alphabet);
        Decryptor decryptor = new Decryptor(alphabet);

        Scanner scanner = new Scanner(System.in);
        String input = "";
        do {
            System.out.println("Sisesta käsklus:");
            input = scanner.nextLine();
            input = input.toUpperCase();
            if (input.startsWith("ENCRYPT")) {
                String textToEncrypt = input.substring(7).trim();
                System.out.println("Sinu sisend oli: " + textToEncrypt);
                System.out.println("Krüpteeritud tekst: " + encryptor.convert(textToEncrypt));
            } else if (input.startsWith("DECRYPT")) {
                String textToDecrypt = input.substring(7).trim();
                System.out.println("Sinu sisend oli: " + textToDecrypt);
                System.out.println("Krüpteeritud tekst: " + decryptor.convert(textToDecrypt));
            }
        } while (!input.equals("EXIT"));
        System.out.println("Head aega!");

//        String initialText = args[1].toUpperCase(); // "VÕTI ON MATI ALL"
//        String encryptedText = encryptor.convert(initialText);
//        System.out.println(encryptedText);
//        String decryptedText = decryptor.convert(encryptedText);
//        System.out.println(decryptedText);
    }
}
