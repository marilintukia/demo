package valiit.cryptoapp;

import java.util.HashMap;
import java.util.Map;

public abstract class Cryptor {
    protected Map<String, String> cryptionMap = new HashMap<>();

    public String convert(String initialText) {
        String convertedText = "";
        for (int i = 0; i < initialText.length(); i++) {
            char letterChar = initialText.charAt(i);
            String letterStr = String.valueOf(letterChar);
            String convertedLetter = this.cryptionMap.get(letterStr);
            convertedText += convertedLetter;
        }
        return convertedText;
    }
}
