package valiit.cryptoapp;

import java.util.List;

public class Encryptor extends Cryptor{
    public Encryptor(List<String> alphabet) {
        for (String letters : alphabet) {
            String[] splitLetters = letters.split(", ");
            this.cryptionMap.put(splitLetters[0], splitLetters[1]);
        }
    }
}
