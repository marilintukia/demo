package valiit.cryptoapp;

import java.util.List;

public class Decryptor extends Cryptor {
    public Decryptor(List<String> alphabet) {
        for (String letters : alphabet) {
            String[] splitLetters = letters.split(", ");
            this.cryptionMap.put(splitLetters[1], splitLetters[0]);
        }
    }
}
