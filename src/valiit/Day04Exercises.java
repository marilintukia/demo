package valiit;

public class Day04Exercises {
    public static void main(String[] args) {
        // Ülesanne 11
        int i = 1;
        while (i <= 100) {
            System.out.println(i);
            i++;
        }

        // Ülesanne 12
        for (int c = 1; c < 101; c++) {
            System.out.println(c);
        }

        // Ülesanne 13
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int number : numbers) {
            System.out.println(number);
        }

        // Ülesanne 14
//        for(int k = 1; k <= 100; k++) {
//            if (k % 3 == 0) {
//                System.out.println(k);
//            }
//        }

        for (int m = 3; m <= 100; m = m + 3) {
            System.out.println(m);
        }
        // Ülesanne 15
        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
        String bandsText = "";
//        for(int n = 0; n < bands.length - 1; n++) {
//            bandsText = bandsText + bands[n] + ", ";
//        }
//        bandsText = bandsText + bands[bands.length - 1];
//        System.out.println(bandsText);
        // bandsText.equals("Sun, Metsatöll, Queen, Metallica")

//        bandsText = String.join(", ", bands);
//        System.out.println(bandsText);

        bandsText = "";
        for (int o = 0; o < bands.length; o++) {
            bandsText = bandsText + bands[o];
            if (o < bands.length - 1) {
                bandsText = bandsText + ", ";
            }
        }
        System.out.println(bandsText);

        // Ülesanne 16
        bandsText = "";
        for (int o = bands.length - 1; o >= 0; o--) {
            bandsText = bandsText + bands[o] + ", ";
        }
        if (bands.length > 0) {
            bandsText = bandsText.substring(0, bandsText.length() - 2);
        }
        System.out.println(bandsText);

        // Ülesanne 18
        double randomNumber = 0;
        do {
            System.out.println("tere");
            randomNumber = Math.random();
        } while (randomNumber < 0.5);

    }
}

