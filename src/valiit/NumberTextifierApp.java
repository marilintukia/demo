package valiit;

public class NumberTextifierApp {
    public static void main(String[] args) {

//        // Lahendus 1 (massiiv)
//        String[] numberWords = {
//                "Null", "Üks", "Kaks", "Kolm", "Neli", "Viis",
//                "Kuus", "Seitse", "Kaheksa", "Üheksa", "Kümme"
//        };
//        String text = "";
//        for(int i = 0; i < args.length; i++) {
//            int number = Integer.parseInt(args[i]);
//            text = text + numberWords[number] + ", ";
//        }
//        if (args.length > 0) {
//            text = text.substring(0, text.length() - 2);
//        }
//        System.out.println(text);

        // Lahendus 2 (switch)
        String text2 = "";
        for(String arg : args) {
            switch (arg) {
                case "0":
                    text2 = text2 + "null, ";
                    break;
                case "1":
                    text2 = text2 + "üks, ";
                    break;
                case "2":
                    text2 = text2 + "kaks, ";
                    break;
                case "3":
                    text2 = text2 + "kolm, ";
                    break;
                case "4":
                    text2 = text2 + "neli, ";
                    break;
                case "5":
                    text2 = text2 + "viis, ";
                    break;
                case "6":
                    text2 = text2 + "kuus, ";
                    break;
                case "7":
                    text2 = text2 + "seitse, ";
                    break;
                case "8":
                    text2 = text2 + "kaheksa, ";
                    break;
                case "9":
                    text2 = text2 + "üheksa, ";
                    break;
            }
        }
        if (args.length > 0) {
            text2 = text2.substring(0, text2.length() - 2);
        }
        System.out.println(text2);
    }
}
