package valiit;

public class Day06Exercises {
    public static void main(String[] args) {

        int i = 0;
        Day06Exercises xyz = new Day06Exercises();

        System.out.println(test(5));
        test2(null, null);
        System.out.println("Toode omahinnaga 28.99. Lõpphind: " + addVat(28.99));
        int[] composedArray = composeArray(1, 2, true);
        for (int x : composedArray) {
            System.out.println(x);
        }

        // Ülesanne 6
        String gender = deriveGender("49403136526"); // "F"
        System.out.println(gender);

        // Ülesanne 7
        int personBirthYear = deriveBirthYear("49403136526"); // 1900 + 94 = 1994
        System.out.println(personBirthYear);

        // Ülesanne 8
        System.out.println(validatePersonalCode("49403136526"));
    }

    // Ülesanne 1
    public static boolean test(int number) {
        return false;
    }

    // Ülesanne 2
    private static void test2(String s1, String s2) {

    }

    // Ülesanne 3
    public static double addVat(double initialPrice) {
        return 1.2 * initialPrice;
    }

    // Ülesanne 4
    public static int[] composeArray(int a, int b, boolean c) {
        int[] array = {a, b};
        return array;
    }

    // Ülesanne 6
    public static String deriveGender(String personalCode) {
        int initialDigit = Integer.parseInt(String.valueOf(personalCode.charAt(0)));
        if (initialDigit % 2 == 0) {
            return "F";
        } else {
            return "M";
        }
    }

    // Ülesanne 7
    public static int deriveBirthYear(String personalCode) {
        char initialDigitChar = personalCode.charAt(0);
        String initialDigitString = String.valueOf(initialDigitChar);
        int initialDigit = Integer.parseInt(initialDigitString);
        int birthYearDigits = Integer.parseInt(personalCode.substring(1, 3));

        int century = 0;
        switch (initialDigit) {
            case 1:
            case 2:
                century = 1800;
                break;
            case 3:
            case 4:
                century = 1900;
                break;
            case 5:
            case 6:
                century = 2000;
                break;
            case 7:
            case 8:
                century = 2100;
                break;
            default:
                century = 0;
        }

        return century + birthYearDigits;
    }

    public static boolean validatePersonalCode(final String personalCode) {
        String firstDigits = personalCode.substring(0, 10);
        String[] digitStrings = firstDigits.split("");
        int[] digits = new int[10];
        for (int i = 0; i < digitStrings.length; i++) {
            digits[i] = Integer.parseInt(digitStrings[i]);
        }

        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};

        int sum = 0;
        for (int i = 0; i < 10; i++) {
            sum = sum + weights1[i] * digits[i];
        }

        int personalCodeLastDigit = Integer.parseInt(String.valueOf(personalCode.charAt(10)));

        int checkDigit = sum % 11;
        if (checkDigit != 10) {
            return checkDigit == personalCodeLastDigit;
        } else {
            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            sum = 0;
            for(int i = 0; i < 10; i++) {
                sum += weights2[i] * digits[i];
            }
            checkDigit = sum % 11;
            if (checkDigit == 10) {
                checkDigit = 0;
            }
            return checkDigit == personalCodeLastDigit;
        }
    }
}
